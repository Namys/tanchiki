package com.getjavajob.training.web06.message;

import com.getjavajob.training.web06.card.Command;

import java.util.List;

/**
 * Created by Nat on 27.09.2015.
 */

public class ClientMessage {

    private String type;
    List<Command> command;

    public ClientMessage() {
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<Command> getCommand() {
        return command;
    }

    public void setCommand(List<Command> command) {
        this.command = command;
    }

    @Override
    public String toString() {
        return "ClientMessage{" +
                "type='" + type + '\'' +
                ", command=" + command +
                '}';
    }
}

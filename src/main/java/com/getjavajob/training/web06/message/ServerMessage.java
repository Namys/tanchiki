package com.getjavajob.training.web06.message;

import java.util.Map;

/**
 * Created by Nat on 26.09.2015.
 */
public class ServerMessage {

    private int clientID;
    private int mapRows;
    private int mapColumns;
    private int[][] map;
    private Map<Integer, Integer> score;

    public ServerMessage() {
    }

    public int getClientID() {
        return clientID;
    }

    public void setClientID(int clientID) {
        this.clientID = clientID;
    }

    public int getMapRows() {
        return mapRows;
    }

    public void setMapRows(int mapRows) {
        this.mapRows = mapRows;
    }

    public int getMapColumns() {
        return mapColumns;
    }

    public void setMapColumns(int mapColumns) {
        this.mapColumns = mapColumns;
    }

    public int[][] getMap() {
        return map;
    }

    public void setMap(int[][] map) {
        this.map = map;
    }

    public Map<Integer, Integer> getScore() {
        return score;
    }

    public void setScore(Map<Integer, Integer> score) {
        this.score = score;
    }

}

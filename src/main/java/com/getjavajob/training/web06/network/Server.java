package com.getjavajob.training.web06.network;

import com.getjavajob.training.web06.card.Command;
import com.getjavajob.training.web06.card.Performer;
import com.getjavajob.training.web06.card.Tables;
import com.getjavajob.training.web06.message.ClientMessage;
import com.getjavajob.training.web06.message.ServerMessage;
import com.getjavajob.training.web06.validation.Validator;
import com.getjavajob.training.web06.xmlserializer.Serializer;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Namys on 26.09.2015.
 */
public class Server {
    public static final int WAR_MAP_SIZE = 10;
    public static final int BARRIER_QTY = 30;

    ServerSocket serverSocket;
    ArrayList<Socket> socketsToSpeakWithClient = new ArrayList<>();
    ArrayList<Socket> socketsUI = new ArrayList<>();
    HashMap<Integer, DataInputStream> socketDataInputStreamHashMap = new HashMap<>();
    HashMap<Integer, DataOutputStream> socketDataOutputStreamHashMap = new HashMap<>();
    boolean haveSocket;
    private int indexCurrentSocket;

    public Server(int port) throws IOException {
        this.serverSocket = new ServerSocket(port);
        this.serverSocket.setSoTimeout(1000);
    }

    public Server() throws IOException {
        this(20000);
    }

    public static void main(String[] args) throws IOException {
        Server server = new Server();
        Serializer serializer = new Serializer();
        Validator validator=new Validator();
        Performer performer = new Performer(WAR_MAP_SIZE);
        Tables tables=new Tables();
        tables.setWarMap(performer.initWarMap(WAR_MAP_SIZE, BARRIER_QTY));
        performer.setWarMap(tables.getWarMap());
        while (true) {    
            if (server.waitClient()){
                System.out.println("new client");
            }
            String xml = server.parseXmlString();
            if (xml!=null) {
                System.out.println(xml);
                ClientMessage clientMessage;
                try {
                    int id = server.getCurrentSocketIndex()+1;
                    clientMessage=serializer.fromXML(xml);
                    List<Command> validatedCommands = validator.validate(clientMessage, tables, id);
                    System.out.println(validatedCommands);
//                    server.sendMessageALL("<serverMessage>\n" +
//                        "\t<clientId>1150</clientId>\n" +
//                        "\t<map rows=\"4\" columns=\"4\">0 10 0 0 10 0 0 0 0 0 0 0 0 10 0 0}</map>\n" +
//                        "\t<scores>\n" +
//                        "\t\t<client>\n" +
//                        "\t\t\t<id>1150</id>\n" +
//                        "\t\t\t<score>2</score>\n" +
//                        "\t\t</client>\n" +
//                        "\t\t<client>\n" +
//                        "\t\t\t<id>1130</id>\n" +
//                        "\t\t\t<score>28</score>\n" +
//                        "\t\t</client>\n" +
//                        "\t\t<client>\n" +
//                        "\t\t\t<id>1650</id>\n" +
//                        "\t\t\t<score>21</score>\n" +
//                        "\t\t</client>\n" +
//                        "\t</scores>\n" +
//                        "</serverMessage>");
                    //from table
                    performer.setWarMap(tables.getWarMap());
                    performer.setScores(tables.getScores());
                    performer.executeCommands(validatedCommands);
                    //make serverMessage
                    ServerMessage serverMessage = new ServerMessage();
                    serverMessage.setClientID(id);
                    serverMessage.setScore(performer.getScores());
                    serverMessage.setMapRows(WAR_MAP_SIZE);
                    serverMessage.setMapColumns(WAR_MAP_SIZE);
                    serverMessage.setMap(performer.getWarMap());
                    //to table
                    tables.setScores(performer.getScores());
                    tables.setWarMap(performer.getWarMap());
                    //sent xml
                    server.sendMessageALL(new Serializer().toXML(serverMessage));
                }catch (Exception e){
                    continue;
                }
            }
        }
    }

    private String parseXmlString() throws IOException {
        DataInputStream streamIn;
        while (true) {
            if (this.haveSocket) {
                streamIn = this.listenSocket();
                String line = "";
                boolean contains=false;
                while (!(contains)) {
                    line += streamIn.readUTF();
                    contains = line.contains("<clientMessage") && line.contains("</clientMessage>");
                }
                line = ("<clientMessage" + line.split("<clientMessage")[1]).split("</clientMessage>")[0] + "</clientMessage>";
                if (line.contains("type=\"ui\"")) {
                    int index = indexCurrentSocket - 1;
                    index=index<0?socketsToSpeakWithClient.size()-1:index;
                    socketsUI.add(socketsToSpeakWithClient.get(index));
                    socketsToSpeakWithClient.remove(index);
                }
                if (contains)
                    return line;
            }
            else {
                return null;
            }
        }
    }
    public int getCurrentSocketIndex() {
        int index = indexCurrentSocket - 1;
        return index<0?socketsToSpeakWithClient.size()-1:index+1;
    }
    public boolean waitClient() {
        Socket accept ;
        try {
            accept = this.serverSocket.accept();
            haveSocket = true;
        } catch (IOException e) {
            return false;
        }
        if (accept != null) {
            socketsToSpeakWithClient.add(accept);
            return true;
        }
        return false;
    }

    public DataInputStream listenSocket() throws IOException {
        if (socketsToSpeakWithClient.size() > 0) {
            Socket socket = socketsToSpeakWithClient.get(indexCurrentSocket);
            if (!socket.isConnected()){
                afterListen();
                return null;
            }
            DataInputStream dataInputStream;
            if (socketDataInputStreamHashMap.containsKey(socket)) {
                dataInputStream = socketDataInputStreamHashMap.get(indexCurrentSocket);
            } else {
                dataInputStream = new DataInputStream(new BufferedInputStream(socket.getInputStream()));
                socketDataInputStreamHashMap.put(indexCurrentSocket, dataInputStream);
                dataInputStream = new DataInputStream(new BufferedInputStream(socket.getInputStream()));
                socketDataInputStreamHashMap.put(indexCurrentSocket, dataInputStream);
            }
            afterListen();
            return dataInputStream;
        }
        return null;
    }

    private void afterListen() {
        indexCurrentSocket++;
        if (indexCurrentSocket > socketsToSpeakWithClient.size() - 1) {
            indexCurrentSocket = 0;
        }
    }

    public void sendMessageALL(String xml) throws IOException {
        for (int i=0;i<socketsToSpeakWithClient.size();i++){
            sendMessage(xml, socketsToSpeakWithClient.get(i),i+1);
        }
        for (int i=0;i<socketsUI.size();i++){
            if (!socketsUI.get(i).isConnected())
            sendMessage(xml, socketsUI.get(i),0);
        }
    }

    private void sendMessage(String xml, Socket socket,int id) throws IOException {
        if (!socket.isConnected()){
            return;
        }
        xml.replace("<clientId>CLIENT_ID</clientId>","<clientId>CLIENT_ID</clientId>");
        String[] strings=xml.split("<clientId>");
        String xmlFix=strings[0]+"<clientId>"+String.valueOf(id)+"</clientId>"+strings[1].split("</clientId>")[1];
        OutputStream outputStream = socket.getOutputStream();
        DataOutputStream dataOutputStream;
        if (socketDataOutputStreamHashMap.containsKey(id)) {
            dataOutputStream= socketDataOutputStreamHashMap.get(id);
        }
        else {
            dataOutputStream= new DataOutputStream(outputStream);
            socketDataOutputStreamHashMap.put(id,dataOutputStream);
        }
        dataOutputStream.write(xmlFix.getBytes());
        dataOutputStream.flush();
    }
}

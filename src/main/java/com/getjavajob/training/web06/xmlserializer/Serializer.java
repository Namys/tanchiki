package com.getjavajob.training.web06.xmlserializer;

import com.getjavajob.training.web06.card.Command;
import com.getjavajob.training.web06.message.ClientMessage;
import com.getjavajob.training.web06.message.ServerMessage;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

import java.util.Map;

/**
 * Created by Nat on 26.09.2015.
 */
public class Serializer {

    private static final String DECLARATION = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
    public static final String ROOT_TAG = "serverMessage";

    public String toXML(Object obj) {
        int clientId = objectToServerMessage(obj).getClientID();
        int rows = objectToServerMessage(obj).getMapRows();
        int columns = objectToServerMessage(obj).getMapColumns();
        String map = arrayToString(objectToServerMessage(obj).getMap());
        Map<Integer, Integer> score = objectToServerMessage(obj).getScore();
        StringBuilder xml = new StringBuilder();
        xml.append(DECLARATION);
        xml.append(getOpenTag(ROOT_TAG)).append("\n");
        xml.append(getTab(1)).append(getTag("clientId", String.valueOf(clientId))).append("\n");
        xml.append(getTab(1)).append("<map ").append("rows=").append("\"").append(rows).append("\"").
                append(" columns=").append("\"").append(columns).append("\"").
                append(">").append(map).append(getCloseTag("map")).append("\n");
        xml.append(getTab(1)).append(getOpenTag("scores")).append("\n");
        for (Map.Entry<Integer, Integer> entry : score.entrySet()) {
            xml.append(getTab(2)).append(getOpenTag("client")).append("\n");
            xml.append(getTab(3)).append(getOpenTag("id")).append(entry.getKey()).append(getCloseTag("id")).append("\n");
            xml.append(getTab(3)).append(getOpenTag("score")).append(entry.getValue()).append(getCloseTag("score")).append("\n");
            xml.append(getTab(2)).append(getCloseTag("client")).append("\n");
        }
        xml.append(getTab(1)).append(getCloseTag("scores")).append("\n");
        xml.append(getCloseTag(ROOT_TAG));
        return xml.toString();
    }

    private String getOpenTag(String tagName) {
        StringBuilder sb = new StringBuilder();
        sb.append("<").append(tagName).append(">");
        return sb.toString();
    }

    private String getCloseTag(String tagName) {
        StringBuilder sb = new StringBuilder();
        sb.append("</").append(tagName).append(">");
        return sb.toString();
    }

    private String getTag(String tagName, String content) {
        StringBuilder sb = new StringBuilder();
        sb.append(getOpenTag(tagName)).append(content).append(getCloseTag(tagName));
        return sb.toString();
    }

    private String getTab(int level) {
        if (level <= 0) {
            return "";
        }
        return String.format("%" + level + "s", "").replace(' ', '\t');
    }

    public ServerMessage objectToServerMessage(Object obj) {
        return (ServerMessage) obj;
    }

    public ClientMessage fromXML(String xml) {
        XStream xstream = new XStream(new DomDriver());
        xstream.alias("clientMessage", ClientMessage.class);
        xstream.alias("command", Command.class);
        xstream.aliasAttribute(ClientMessage.class, "type", "type");
        xstream.aliasAttribute(Command.class, "row", "row");
        xstream.aliasAttribute(Command.class, "col", "col");
        xstream.aliasAttribute(Command.class, "type", "type");
        xstream.addImplicitCollection(ClientMessage.class, "command");
        xstream.processAnnotations(Command.class);
        xstream.processAnnotations(ClientMessage.class);
        return (ClientMessage) xstream.fromXML(xml);
    }

    public String arrayToString(int[][] numbers) {
        StringBuilder sb = new StringBuilder();
        for (int[] number : numbers) {
            for (int i : number) {
                sb.append(i).append(" ");
            }
        }
        return sb.toString();
    }
}

package com.getjavajob.training.web06.validation;

import com.getjavajob.training.web06.card.Client;
import com.getjavajob.training.web06.card.Command;
import com.getjavajob.training.web06.card.Tables;
import com.getjavajob.training.web06.card.Tank;
import com.getjavajob.training.web06.message.ClientMessage;

import java.util.*;

public class ValidatorTest {

    public static void main(String[] args) {

        //fill ClientMessage
        List<Command> commands = new ArrayList<>();
//        commands.add(new Command(0, 1, "fire"));
        commands.add(new Command(2, 1, "fire"));
        commands.add(new Command(1, 0, "moveForward"));
        commands.add(new Command(3, 1, "moveForward"));
        commands.add(new Command(3, 1, "moveForward"));
        commands.add(new Command(3, 1, "moveForward"));
        commands.add(new Command(3, 1, "moveForward"));
        commands.add(new Command(3, 1, "moveForward"));

        ClientMessage clientMessage = new ClientMessage();
        clientMessage.setCommand(commands);

        //fill tables
        byte[][] warMap = {
                {0, 10, 0, 0},
                {10, 0, 0, 0},
                {0, 0, 0, 0},
                {0, 10, 0, 0}
        };

        // test
        byte[][] copyOfWarMap = warMap.clone();
        System.out.println("copyOfWarMap == warMap = " + (copyOfWarMap == warMap));
        Collection<Tank> tanks = new ArrayList<>();
        tanks.add(new Tank(0, 1, true));
        tanks.add(new Tank(1, 0, true));
        tanks.add(new Tank(3, 1, true));

        Map<Integer, Client> clients = new HashMap<>();
        clients.put(1, new Client(1, tanks));

        Tables table = new Tables(warMap, null, clients);

        System.out.println(Arrays.deepToString(table.getWarMap()));

        Validator validator = new Validator();
        List<Command> validCommands = validator.validate(clientMessage, table, 1);
        for (Command c : validCommands) {
            System.out.println(c);
        }
        System.out.println(Arrays.deepToString(table.getWarMap()));
    }
}

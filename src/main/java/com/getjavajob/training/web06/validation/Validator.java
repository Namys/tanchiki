package com.getjavajob.training.web06.validation;

import com.getjavajob.training.web06.card.Command;
import com.getjavajob.training.web06.card.Tables;
import com.getjavajob.training.web06.card.Tank;
import com.getjavajob.training.web06.message.ClientMessage;

import java.util.ArrayList;
import java.util.List;

public class Validator {
    private final static int NORTH = 0;
    private final static int EAST = 1;
    private final static int SOUTH = 2;
    private final static int WEST = 3;

    public List<Command> validate(ClientMessage clientMessage, Tables table, int clientsID) {
        List<Tank> clientsTanks = new ArrayList<>();
        int[][] warMap = table.getWarMap();
        int colLength=warMap[0].length;
        for (int i=0;i<warMap.length;i++){
            for (int i1=0;i1<colLength;i1++) {
                if (warMap[i][i1]/10==clientsID)
                    clientsTanks.add(new Tank(i,i1,true));
            }
        }
        List<Command> validCommands = new ArrayList<>();
        // get copy of game Map
        int warMapLength = warMap.length;
        int[][] warMapsCopy = new int[warMapLength][warMapLength];
        for (int i = 0; i < warMapLength; i++) {
            for (int j = 0; j < warMapLength; j++) {
                warMapsCopy[i][j] = warMap[i][j];
            }
        }
        for (Command command : clientMessage.getCommand()) {
            String cType = command.getType();
            // all tanks have commands
            if (clientsTanks.isEmpty()) {
                return validCommands;
            }
            for (Tank tank : clientsTanks) {
                if (command.getRow() == tank.getRow() && command.getCol() == tank.getCol()) {
                    if (cType.equals("turnLeft") || cType.equals("turnRight")
                            || cType.equals("moveForward") || cType.equals("fire") || cType.equals("skip")) {
                        if (cType.equals("moveForward")) {
                            if (!validateCommand(tank, warMapsCopy)) {
                                command.setType("skip");
                            }
                        }
                    } else {
                        command.setType("skip");
                    }
                    validCommands.add(command);
                    clientsTanks.remove(tank);
                    break;
                }
            }
        }
        // enter to this loop if some tanks was missed
        for (Tank tank : clientsTanks) {
            validCommands.add(new Command(tank.getRow(), tank.getCol(), "skip"));
        }
        return validCommands;
        }

    private boolean validateCommand(Tank tank, int[][] gameMap) {
        int row = tank.getRow();
        int col = tank.getCol();
        int direction = gameMap[row][col] % 10;
        switch (direction) {
            case SOUTH:
                if (row != gameMap.length - 1 && gameMap[row + 1][col] == 0) {
                    gameMap[row + 1][col] = gameMap[row][col];
                    gameMap[row][col] = 0;
                    return true;
                }
                break;
            case NORTH:
                if (row != 0 && gameMap[row - 1][col] == 0) {
                    gameMap[row - 1][col] = gameMap[row][col];
                    gameMap[row][col] = 0;
                    return true;
                }
                break;
            case EAST:
                if (row != gameMap.length - 1 && gameMap[row][col + 1] == 0) {
                    gameMap[row][col + 1] = gameMap[row][col];
                    gameMap[row][col] = 0;
                    return true;
                }
                break;
            case WEST:
                if (row != 0 && gameMap[row][col - 1] == 0) {
                    gameMap[row][col - 1] = gameMap[row][col];
                    gameMap[row][col] = 0;
                    return true;
                }
                break;
        }
        return false;
    }
}

package com.getjavajob.training.web06.card;

import java.util.Collection;

/**
 * Created by ����� on 26.09.2015.
 */
public class Client {
    private int id;
    private Collection<Tank> tanks;

    public Client() {
    }

    public Client(int id, Collection<Tank> tanks) {
        this.id = id;
        this.tanks = tanks;
    }

    public int getId() {
        return id;
    }

    public Collection<Tank> getTanks() {
        return tanks;
    }
}

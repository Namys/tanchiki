package com.getjavajob.training.web06.card;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Намыс on 26.09.2015.
 */
public class Tables {
    private int[][] warMap = {};
    private Map<Integer, Integer> scores = new HashMap<>();// первый int это id клиента , а второй это оге очки
    private Map<Integer, Client> clients = new HashMap<>();

    public Tables() {
    }

    public Tables(int[][] warMap, Map<Integer, Integer> scores, Map<Integer, Client> clients) {
        this.warMap = warMap;
        this.scores = scores;
        this.clients = clients;
    }

    public int[][] getWarMap() {
        return warMap;

    }

    public Map<Integer, Integer> getScores() {
        return scores;
    }

    public Map<Integer, Client> getClients() {
        return clients;
    }

    public void setWarMap(int[][] warMap) {
        this.warMap = warMap;
    }

    public void setScores(Map<Integer, Integer> scores) {
        this.scores = scores;
    }

    public void setClients(Map<Integer, Client> clients) {
        this.clients = clients;
    }
}
package com.getjavajob.training.web06.card;

/**
 * Created by Nat on 27.09.2015.
 */
public class Command {

    private int row;
    private int col;
    private String type;

    public Command() {
    }

    public Command(int row, int col, String type) {
        this.row = row;
        this.col = col;
        this.type = type;
    }

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public int getCol() {
        return col;
    }

    public void setCol(int col) {
        this.col = col;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Command{" +
                "row=" + row +
                ", col=" + col +
                ", type='" + type + '\'' +
                '}';
    }
}

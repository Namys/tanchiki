package com.getjavajob.training.web06.card;

import com.getjavajob.training.web06.message.ServerMessage;
import com.getjavajob.training.web06.xmlserializer.Serializer;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * Created by Nat on 29.09.2015.
 */
public class Performer {

    public static final int WAR_MAP_SIZE = 10;
    public static final int BARRIER_QTY = 30;

    public static void main(String[] args) {
        String[] strings = {"turnLeft", "turnRight", "moveForward", "fire", "skip"};
        Random random = new Random();
        List<List<Command>> listList = new ArrayList<>();
        List<Command> list = new ArrayList<>();
        Performer performer = new Performer(WAR_MAP_SIZE);
        Tables tables = new Tables();
        tables.setWarMap(performer.initWarMap(WAR_MAP_SIZE, BARRIER_QTY));
        performer.setWarMap(tables.getWarMap());
        for (int i = 1; i < 10; i++) {
            list.add(new Command(i, i, strings[random.nextInt(strings.length - 1)]));
            if (i % 3 == 0) {
                listList.add(list);
                list = new ArrayList<>();
            }
        }
        int i = 0;
        boolean init = true;
        while (true) {
            int id = 0;
            id = i + 1;
            if (init) {
                //здесь инициализация клиентов
                System.out.println("Init client: " + id);
                System.out.println(performer.initClient(tables, id, WAR_MAP_SIZE));
                tables.setWarMap(performer.getWarMap());
                init = false;
            } else {
                List<Command> commands = listList.get(i);
                //здесь должна бытьобработка команд и возвращает XML
                //from table
                performer.setWarMap(tables.getWarMap());
                performer.setScores(tables.getScores());
                performer.executeCommands(commands);
                //make serverMessage
                ServerMessage serverMessage = new ServerMessage();
                serverMessage.setClientID(id);
                serverMessage.setScore(performer.getScores());
                serverMessage.setMapRows(WAR_MAP_SIZE);
                serverMessage.setMapColumns(WAR_MAP_SIZE);
                serverMessage.setMap(performer.getWarMap());
                //to table
                tables.setScores(performer.getScores());
                tables.setWarMap(performer.getWarMap());
                System.out.println("Make command client: " + id);
                System.out.println(new Serializer().toXML(serverMessage));
                i++;
                if (i > listList.size() - 1)
                    i = 0;
                init = true;
            }
        }
    }

    private int warMapSize;
    private int[][] warMap;
    private java.util.Map<Integer, Integer> scores;

    public Performer(int warMapSize) {
        this.warMapSize = warMapSize;
    }

    public Performer(int[][] warMap, Map<Integer, Integer> scores, int warMapSize) {
        this.warMap = warMap;
        this.scores = scores;
        this.warMapSize = warMapSize;
    }

    public void setWarMap(int[][] warMap) {
        this.warMap = warMap;
    }

    public void setScores(Map<Integer, Integer> scores) {
        this.scores = scores;
    }

    public int[][] getWarMap() {
        return warMap;
    }

    public Map<Integer, Integer> getScores() {
        return scores;
    }

    public int getWarMapSize() {
        return warMapSize;
    }

    public void setWarMapSize(int warMapSize) {
        this.warMapSize = warMapSize;
    }

    public void executeCommands(List<Command> commands) {
        if (commands == null) {
            return;
        }
        for (Command command : commands) {
            if (command.getType().equals("skip")) {
                continue;
            }
            int m = command.getRow();
            int n = command.getCol();
            int tank = warMap[m][n];
            switch (command.getType()) {
                case "turnLeft":
                    warMap[m][n] = turnLeft(tank);
                    break;
                case "turnRight":
                    warMap[m][n] = turnRight(tank);
                    break;
                case "moveForward":
                    moveForward(tank, m, n);
                    break;
                case "fire":
                    fire(tank, m, n);
                    break;
            }
        }
    }

    private void fire(int tank, int m, int n) {
        int direction = tank % 10;
        int clientID = tank / 10;
        switch (direction) {
            case 0:
                if (m > 0) {
                    for (int i = m - 1; i >= 0; i--) {
                        if (warMap[i][n] < 0) {
                            break;
                        }
                        if (warMap[i][n] > 0) { //tank destroyed
                            incScore(clientID);
                            placeNewTankOnWarMap(warMapSize, warMap[i][n] / 10);
                            destroyTank(i, n);
                            break;
                        }
                    }
                }
                break;
            case 1:
                if (n < warMapSize - 1) {
                    for (int i = n + 1; i < warMapSize; i++) {
                        if (warMap[m][i] < 0) {
                            break;
                        }
                        if (warMap[m][i] > 0) {
                            incScore(clientID);
                            placeNewTankOnWarMap(warMapSize, warMap[m][i] / 10);
                            destroyTank(m, i);
                            break;
                        }
                    }
                }
                break;
            case 2:
                if (m < warMapSize - 1) {
                    for (int i = m + 1; i < warMapSize; i++) {
                        if (warMap[i][n] < 0) {
                            break;
                        }
                        if (warMap[i][n] > 0) {
                            incScore(clientID);
                            placeNewTankOnWarMap(warMapSize, warMap[i][n] / 10);
                            destroyTank(i, n);
                            break;
                        }
                    }
                }
                break;
            case 3:
                if (n > 0) {
                    for (int i = n - 1; i >= 0; i--) {
                        if (warMap[m][i] < 0) {
                            break;
                        }
                        if (warMap[m][i] > 0) {
                            incScore(clientID);
                            placeNewTankOnWarMap(warMapSize, warMap[m][i] / 10);
                            destroyTank(m, i);
                            break;
                        }
                    }
                }
                break;
        }
    }

    private void destroyTank(int m, int n) {
        warMap[m][n] = 0;
    }

    private void incScore(int clientID) {
        int newScore = scores.get(clientID) == null ? 1 : scores.get(clientID) + 1;
        scores.put(clientID, newScore);
    }

    private void moveForward(int tank, int m, int n) {
        int direction = tank % 10;
        switch (direction) {
            case 0:
                if (m > 0 && warMap[m - 1][n] == 0) {
                    warMap[m - 1][n] = tank;
                    warMap[m][n] = 0;
                }
                break;
            case 1:
                if (n < warMapSize - 1 && warMap[m][n + 1] == 0) {
                    warMap[m][n + 1] = tank;
                    warMap[m][n] = 0;
                }
                break;
            case 2:
                if (m < warMapSize - 1 && warMap[m + 1][n] == 0) {
                    warMap[m + 1][n] = tank;
                    warMap[m][n] = 0;
                }
                break;
            case 3:
                if (n > 0 && warMap[m][n - 1] == 0) {
                    warMap[m][n - 1] = tank;
                    warMap[m][n] = 0;
                }
                break;
        }
    }

    private int turnLeft(int tank) {
        int direction = tank % 10;
        direction = (direction == 0) ? 3 : direction - 1;
        return tank / 10 * 10 + direction;
    }

    private int turnRight(int tank) {
        return tank / 10 * 10 + (tank % 10 + 1) % 4;
    }

    public void placeNewTankOnWarMap(int warMapSize, int clientID) {
        int newTank = clientID * 10;
        Random random = new Random();
        while (true) {
            int i = random.nextInt(warMapSize);
            int j = random.nextInt(warMapSize);
            if (warMap[i][j] == 0) {
                warMap[i][j] = newTank;
                break;
            }
        }
    }

    public void placeAllNewTanksOnWarMap(int tanksNumber, int warMapSize, int clientID) {
        for (int i = 0; i < tanksNumber; i++) {
            this.placeNewTankOnWarMap(warMapSize, clientID);
        }
    }

    public int[][] initWarMap(int warMapSize, int barrierQty) {
        int[][] newWarMap = new int[warMapSize][warMapSize];
        Random random = new Random();
        int bQty = 0;
        while (bQty <= barrierQty) {
            int i = random.nextInt(warMapSize);
            int j = random.nextInt(warMapSize);
            if (newWarMap[i][j] == 0) {
                newWarMap[i][j] = -1;
                bQty++;
            }
        }
        return newWarMap;
    }

    public Tables initTable(int warMapSize, int barrierQty) {
        Tables table = new Tables();
        table.setWarMap(this.initWarMap(warMapSize, barrierQty));
        return table;
    }

    public String initClient(Tables tables, int clientID, int warMapSize) {
        tables.getScores().put(clientID, 0);
        Performer performer = new Performer(warMapSize);
        performer.setWarMap(tables.getWarMap());
        performer.setScores(tables.getScores());
        performer.placeAllNewTanksOnWarMap(3, warMapSize, clientID);
        ServerMessage serverMessage = new ServerMessage();
        serverMessage.setScore(performer.getScores());
        serverMessage.setMap(performer.getWarMap());
        serverMessage.setMapRows(warMapSize);
        serverMessage.setMapColumns(warMapSize);
        serverMessage.setClientID(clientID);
        tables.setWarMap(performer.getWarMap());
        tables.setScores(performer.getScores());
        return new Serializer().toXML(serverMessage);
    }
}
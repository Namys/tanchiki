package com.getjavajob.training.web06.card;

/**
 * Created by ����� on 26.09.2015.
 */
public class Tank {
    private int row;
    private int col;
    private boolean aLive = true;

    public Tank(int row, int col, boolean aLive) {
        this.row = row;
        this.col = col;
        this.aLive = aLive;
    }

    public int getRow() {
        return row;
    }

    public int getCol() {
        return col;
    }

    public boolean isaLive() {
        return aLive;
    }
}
